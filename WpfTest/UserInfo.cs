﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest
{
    public class UserInfo
    {
        public string UID { get; set; }
        public string UNAME { get; set; }
        public string BackupTime { get; set; }
        public string USERSAVEGAME { get; set; }
    }
}
