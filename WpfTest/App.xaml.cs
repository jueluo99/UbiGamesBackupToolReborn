﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace WpfTest
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //base.OnStartup(e);
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            try {
                if (e.Args.Length == 1)
                {
                    Console.WriteLine(e.Args.Length);
                    MainWindow window = new MainWindow();
                    window.Show();
                    window.Visibility = Visibility.Hidden;
                }
                else
                {
                    //new MainWindow() { WindowState = WindowState.Minimized, WindowStartupLocation = WindowStartupLocation.CenterScreen }.Show();
                    new MainWindow().Show();
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //String resourceName = "UbiGamesBackupTool." + new AssemblyName(args.Name).Name + ".dll";
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("UbiGamesBackupTool.Newtonsoft.Json.dll"))
            {
                Byte[] assemblyData = new Byte[stream.Length];
                stream.Read(assemblyData, 0, assemblyData.Length);
                return Assembly.Load(assemblyData);
            }
            throw new NotImplementedException();
        }
    }
}
