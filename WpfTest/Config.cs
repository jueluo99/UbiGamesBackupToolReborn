﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTest;

namespace UbiGamesBackupTool
{
    class Config
    {
        private bool allowbackup = false;
        public bool AllowBackup { get { return allowbackup; } set { allowbackup = value; } }
        private string allowbackuppath = System.AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar+"AllowBackup";
        public string AllowBackupPath
        {
            get { return allowbackuppath; }
            set
            {
                allowbackuppath = value;
            }
        }
        private string allowbackupshowlocation = "TopLeft";
        public string AllowBackupShowLocation
        {
            get { return allowbackupshowlocation; }
            set
            {
                allowbackupshowlocation = value;
            }
        }

        private bool rungametipstatus = true;
        public bool RunGameTipStatus {
            get {
                return rungametipstatus;
            }
            set {
                rungametipstatus = value;
            }
        }
        private bool exitgametipstatus = true;
        public bool ExitGameTipStatus {
            get {
                return exitgametipstatus;
            }
            set {
                exitgametipstatus = value;
            }
        }

        public event EventHandler PropertyChanged;

        public void Save() {
            if (this != null)
            {
                using (Stream stream = new FileStream(MainWindow.ConfigFilePath, FileMode.Create))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.WriteLine(JsonConvert.SerializeObject(this));
                    }
                }
            }
        }
    }
}
