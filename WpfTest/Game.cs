﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest
{
    public class Game
    {
        public string id { get; set; }
        public string name { get; set; }
        public string img { get; set; }
        public string Version { get; set; }
        public string Title { get; set; }
        public string AppName { get; set; }
    }
}
