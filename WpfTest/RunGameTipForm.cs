﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UbiGamesBackupTool
{
    public partial class RunGameTipForm : Form
    {
        private SynchronizationContext mainThreadSynContext;
        public RunGameTipForm()
        {
            InitializeComponent();
            mainThreadSynContext = SynchronizationContext.Current;
            Console.WriteLine("初始化结束");
        }

        private void RunGameTipForm_Load(object sender, EventArgs e)
        {
            Console.WriteLine("初始化结束");
            Thread thread = new Thread(new ThreadStart(CloseForm));
            thread.IsBackground = true;
            thread.Start();
        }
        public void CloseForm() {
            Thread.Sleep(3000);
            Console.WriteLine("休眠结束");
            mainThreadSynContext.Post(new SendOrPostCallback(OnConnected), null);                      //通知主线程
        }

        private void OnConnected(object state)
        {
            Console.WriteLine("窗体关闭");
            this.Close();
        }
    }
}
