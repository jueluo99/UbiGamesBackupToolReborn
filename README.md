# UbiGamesBackupToolReborn
作品发布页：https://steamcn.com/t353640-1-1

### 现有特性
- 自动探测Uplay安装路径（无需管理员权限）
- 支持检测多个Uplay用户
- 支持选择备份任意数量的游戏
- 还原已经备份的游戏

### 计划中特性
- 自动更新应用
- 激活次数或启动次数
- 实时备份游戏分类(Steam/Uplay)
- Uplay新游戏支持
- 接入第三方云储存备份
- 能够备份一些并不在Uplay安装目录下游戏的备份（例如：《刺客信条：兄弟会》）
- 快捷键的支持
- RPG制作大师游戏存档备份支持
- 全选、全反选等操作

### 待修正
- 《幽灵行动：荒野》 与 《刺客信条：大革命》支持

### BUG
- 暂无

### 文件说明

- UbiGamesBackupTool
    - AllowBackup 默认的实时备份路径
    - config.json 配置文件
    - ListenerGameList.json 实时备份游戏列表
    - UbiGamesBackupTool.exe 备份工具执行文件
- AllowBackup
    - ... 游戏存档备份文件
    - userinfo.json 存档描述文件

- config.json
```
        {
            "AllowBackup":true,                     是否开启实时备份
            "AllowBackupPath":"",                   实时备份存档路径
            "AllowBackupShowLocation":"TopLeft",    实时备份通知显示位置
            "RunGameTipStatus":true,                是否显示游戏启动通知
            "ExitGameTipStatus":true                是否显示游戏退出通知
        }
```

- ListenerGameList.json
```
        [
            {
                "id":"1843",                                    游戏ID号、存档文件夹名称
                "name":"Tom Clancy's Rainbow Six Siege",        游戏名称
                "img":"b1d5066f0d872c26f41b65d70926ddc7.png",   Uplay缓存的游戏LOGO
                "Version":"Steam",                              游戏平台Steam、Uplay、Steam\Uplay
                "Title":"Rainbow Six",                          游戏启动后窗体名称
                "AppName":""                                    游戏可执行文件名称
            },
            ......
        ]
```

- userinfo.json
...
        [
            {
                "UID":"",                                       用户Uplay Uid
                "UNAME":"lzjyzq2",                              用户Uplay 用户名
                "BackupTime":"2018-05-26 22.17.18",             备份时间
                "USERSAVEGAME":""                               备份路径
            },
            ......
        ]
...
### 更新日志
#### v0.2.6
- 修复了多用户状态下，用户头像丢失导致的程序崩溃
- 添加了“全反选”功能
- 支持了更多游戏，添加了更多图片
#### v0.2.5
- 更新了资源文件的引用方式
- 修正开机启动后无法正常还原的BUG
- 优化设计，工具运行时不会显示在任务栏
- 添加 创建快捷方式 功能
#### v0.2.4
- 修正了一直没更新的版本号
#### v0.2.2 v0.2.1
- 新增了对更多游戏的支持
- 修复了无法开机启动的bug
#### v0.2.0
- 使用wpf重构了项目
- 实时备份游戏
- 支持开机自动启动
#### v0.1.1
- 新增还原已备份的存档功能
#### v0.1.0
- 初版发布